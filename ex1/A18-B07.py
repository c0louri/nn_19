from time import time
import warnings
warnings.filterwarnings('ignore')
import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
from sklearn.dummy import DummyClassifier
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report,confusion_matrix,f1_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import cross_val_score, cross_validate, GridSearchCV
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from imblearn.pipeline import Pipeline
from sklearn.preprocessing import MultiLabelBinarizer, StandardScaler, MinMaxScaler
from sklearn.feature_selection import VarianceThreshold
from sklearn.decomposition import PCA
from sklearn.impute import SimpleImputer

# Linux for converting .arff to .csv files
#!cat 5year.arff | grep -ve "^@\|^%" | grep -v "^[[:space:]]*$" > 5year.csv

# Windows
#!findstr/BV "@" 5year.arff | findstr /BV "%" | findstr /V /R /C:"^[]*$"> 5year.csv

# Checking shape of csv files
df1 = pd.read_csv("1year.csv", header=None)
df2 = pd.read_csv("2year.csv", header=None)
df3 = pd.read_csv("3year.csv", header=None)
df4 = pd.read_csv("4year.csv", header=None)
df5 = pd.read_csv("5year.csv", header=None)
print(df1.shape)
print(df2.shape)
print(df3.shape)
print(df4.shape)
print(df5.shape)

# concat all csv files to one
#!cat *year.csv > big.csv #Linux
#copy *year.csv big.csv

# Read final csv file, replace ? with NaN
df = pd.read_csv("big.csv", header = None, na_values=["?"])
print(df.shape)
#print(df)

# next line is needed if Windows
#df = df.drop(df.index[43405]) #remove last row
features = df.iloc[:, :64]
print(features)
labels = np.array(df.iloc[:, 64])
labels = labels.astype(int) #convert to integer values
print (labels)

# Frequencies of classes:
labels_size = labels.shape
counts = np.bincount(labels)
freqs = counts / labels_size
print("Frequency of 0 =",freqs[0]*100,"%")
print("Frequency of 1 =",freqs[1]*100,"%")

# Splitting dataset into training and test set
train, test, train_labels, test_labels = train_test_split(features, labels, test_size = 0.3)
print(train.shape)
print(test.shape)

# Check how many lines have missing values
#linux
#!cat big.csv | grep "?" |  wc -l
#windows
#!findstr "?" data.csv |  find /c /v ""

# Imputing dataset
imp_mean = SimpleImputer(strategy='mean')
imp_mean.fit(train)
train = pd.DataFrame(imp_mean.transform(train))
test = pd.DataFrame(imp_mean.transform(test))

## Baseline Classification
# Dictionary for the results of the Classifiers
baseline_f1_micro = {}
baseline_f1_macro = {}

# Initialize all classifiers with default values
classifiers = [
            ('Uniform', DummyClassifier(strategy='uniform')),
            ('Constant 0', DummyClassifier(strategy='constant', constant=0)),
            ('Constant 1', DummyClassifier(strategy='constant', constant=1)),
            ('Most Frequent', DummyClassifier(strategy='most_frequent')),
            ('Stratified', DummyClassifier(strategy='stratified')),
            ('GNB', GaussianNB()),
            ('kNN', KNeighborsClassifier()),
            ('MLP', MLPClassifier())]

# loop for fit-predict-computing every classifier for baseline 
for (name, cl) in classifiers:
    print(name, "\n")
    model = cl.fit(train, train_labels)
    pred = cl.predict(test)
    # save f1 micro and f1 macro
    baseline_f1_micro[name] = f1_score(test_labels, pred, average="micro")
    baseline_f1_macro[name] = f1_score(test_labels, pred, average="macro")
    # print results:
    print("Confusion matrix:\n" ,confusion_matrix(test_labels, pred), "\n")
    print("F1_micro score:" ,baseline_f1_micro[name])
    print("F1 macro score:" ,baseline_f1_macro[name], "\n")
    print("Metrics Scores and averages:\n", classification_report(test_labels, pred))

## Bar plots for f1 micro and macro
##Firstly for F1 Micro
names = list(baseline_f1_micro.keys())
values = list(baseline_f1_micro.values())
y_pos = np.arange(len(names))
plt.barh(y_pos, values, color = (0.5,0.1,0.5,0.6))
plt.title('F1 Micro')
plt.ylabel('Classifiers')
plt.xlabel('Values')
plt.yticks(y_pos, names)
plt.show()

##Firstly for F1 Macro
names = list(baseline_f1_macro.keys())
values = list(baseline_f1_macro.values())
y_pos = np.arange(len(names))
plt.barh(y_pos, values)
plt.title('F1 Macro')
plt.ylabel('Classifiers')
plt.xlabel('Values')
plt.yticks(y_pos, names)
plt.show()

#####################
## Optimized Classification
####################
# Create a smaller dataset
sdata, starget = shuffle(train, train_labels, random_state = 1000)
samples = 10000
data = sdata.iloc[0:samples-1,:]
# Frequency of classes in the smaller dataset:
target_size = target.shape
counts = np.bincount(target)
freqs = counts / target_size
print("Frequency of 0 =",freqs[0]*100,"%")
print("Frequency of 1 =",freqs[1]*100,"%")

### Code for cross-validation with GridSearchCV for GNB for f1 micro
# with parameter values for the last check
selector = VarianceThreshold()
scaler = StandardScaler()
ros = RandomOverSampler()
pca = PCA()
gnb = GaussianNB()
# Steps of the pipeline
steps = [('selector', selector),
         ('scaler', scaler),
         ('sampler', ros),
         ('pca', pca),
         ('GNB', gnb)
        ]
memory = 'tmp'
# dictionary of values for the parameters
val_dict = dict(
                scaler=[StandardScaler(), None],
                sampler=[RandomOverSampler(), None], 
                pca__n_components=[2, 3, 4, 5]
               )
# Initalizing Pipeline
pipe = Pipeline(steps, memory)
# GridSearchCV
estimator = GridSearchCV(pipe, val_dict, n_jobs=-1, cv=5, scoring='f1_micro')
estimator.fit(train, train_labels)
print(estimator.best_estimator_)   
print(estimator.best_params_)
print()
target = starget[0:samples-1]

### GNB
### Code for cross-validation with GridSearchCV for GNB for f1 macro
# with parameter values for the last check
selector = VarianceThreshold()
scaler = StandardScaler()
ros = RandomOverSampler()
pca = PCA()
gnb = GaussianNB()

# Steps of the pipeline
steps = [('selector', selector),
         ('scaler', scaler),
         ('sampler', ros),
         ('pca', pca),
         ('GNB', gnb)
        ]
memory = 'tmp'

# dictionary of v
# alues for the parameters
val_dict = dict(
                scaler=[StandardScaler(), None],
                sampler=[RandomOverSampler(), None], 
                pca__n_components=[1, 2, 3, 4, 5]
               )


# Initalizing Pipeline
pipe = Pipeline(steps, memory)
# GridSearchCV
estimator = GridSearchCV(pipe, val_dict, n_jobs=-1, cv=5, scoring='f1_macro')
estimator.fit(train, train_labels)
print(estimator.best_estimator_)   
print(estimator.best_params_)
print()

### kNN
### Code for cross-validation with GridSearchCV for kNN for f1 macro
# with parameter values for the last check
selector = VarianceThreshold()
scaler = StandardScaler()
ros = RandomOverSampler()
pca = PCA()
knn = KNeighborsClassifier()
metrics_knn = ['euclidean', 'manhattan', 'chebyshev', 'minkowski']
# Steps of the pipeline
steps = [('selector', selector),
         ('scaler', scaler),
         ('sampler', ros),
         ('pca', pca),
         ('knn', knn)
        ]
memory = 'tmp'
# dictionary of values for the parameters
val_dict = dict(
                scaler=[StandardScaler(), MinMaxScaler(), None],
                sampler=[RandomOverSampler(), None], 
                pca__n_components=[10, 20, 30, 40, 50, 60, 64],
                knn__n_neighbors=[5, 10, 20, 30, 40, 50],
                knn__weights=['uniform', 'distance'],
                knn__metric=metrics_knn
               )
# Initalizing Pipeline
pipe = Pipeline(steps, memory)
# GridSearchCV
estimator = GridSearchCV(pipe, val_dict, n_jobs=-1, cv=5, scoring='f1_micro')
estimator.fit(data, target)
#το παρακάτω αποτέλεσμα είναι για train set μεγέθους <= 1000
print(estimator.best_estimator_)   
print(estimator.best_params_)
print()

### MLP
### Code for cross-validation with GridSearchCV for kNN for f1 macro
# with parameter values for the last check
selector = VarianceThreshold()
scaler = StandardScaler()
ros = RandomOverSampler()
pca = PCA()
knn = KNeighborsClassifier()
metrics_knn = ['euclidean', 'manhattan', 'chebyshev', 'minkowski']
# Steps of the pipeline
steps = [('selector', selector),
         ('scaler', scaler),
         ('sampler', ros),
         ('pca', pca),         
         ('knn', knn)
        ]
memory = 'tmp'
# dictionary of values for the parameters
val_dict = dict(
                scaler=[None],
                sampler=[None], 
                pca__n_components=[28,29],
                knn__n_neighbors=[1,2,3,4,5,6],
                knn__weights=['uniform'],
                knn__metric=['manhattan']
               )
# Initalizing Pipeline
pipe = Pipeline(steps, memory)
# GridSearchCV
estimator = GridSearchCV(pipe, val_dict, n_jobs=-1, cv=5, scoring='f1_macro')
estimator.fit(train, train_labels)
print(estimator.best_estimator_)   
print(estimator.best_params_)
print()

### Best pipelines

# Initializing dictionaries for storing results
final_pipes = {}
final_f1_micro = {}
final_f1_macro = {}
times_s = {}

# Initializing pipelines with best estimated parameters from GridSearchCV
# GNB (same for f1 micro and macro)
gnb_pipe = Pipeline(memory='tmp',
                          steps=[('selector', VarianceThreshold(threshold=0.0)),
                                 ('scaler', StandardScaler(copy=True, with_mean=True, with_std=True)),
                                 ('sampler', RandomOverSampler(random_state=None, ratio=None, return_indices=False, sampling_strategy='auto')),
                                 ('pca', PCA(copy=True, iterated_power='auto', n_components=2, random_state=None, svd_solver='auto', tol=0.0, whiten=False)),
                                 ('GNB', GaussianNB(priors=None, var_smoothing=1e-09))])
final_pipes['GNB'] = gnb_pipe

# We dont need presenting different pipeline for kNN micro because at the baseline is already almost optimal for f1 macro 
# kNN
knn_pipe = Pipeline(memory='tmp',
                    steps=[('selector', VarianceThreshold(threshold=0.0)),
                           ('scaler', None),
                           ('sampler', None),
                           ('pca', PCA(copy=True, iterated_power='auto', n_components=28, random_state=None, svd_solver='auto', tol=0.0, whiten=False)),
                           ('knn', KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='manhattan', metric_params=None, n_jobs=None, n_neighbors=3, p=2, weights='uniform'))])
final_pipes['kNN'] = knn_pipe

# We dont need presenting different pipeline for kNN micro because at the baseline is already almost optimal for f1 macro
# MLP
mlp_pipe = Pipeline(memory='tmp',
                    steps=[('selector', VarianceThreshold(threshold=0.0)),
                           ('scaler', StandardScaler(copy=True, with_mean=True, with_std=True)),
                           ('sampler', None),
                           ('pca', PCA(copy=True, iterated_power='auto', n_components=59, random_state=None, svd_solver='auto', tol=0.0, whiten=False)),
                           ('mlp', MLPClassifier(activation='tanh',alpha=0.0001,hidden_layer_sizes=(28,),learning_rate='constant',max_iter=500, solver='lbfgs'))])
final_pipes['MLP'] = mlp_pipe

# for-loop for fit-predict pipelines with whole dataset
list_classifiers = ['GNB', 'kNN', 'MLP']
for cl in list_classifiers:
    # run pipeline for cl estimator
    t0 = time()
    model = final_pipes[cl].fit(train, train_labels)
    t1 = time()
    pred = final_pipes[cl].predict(test)
    t2 = time()
    # print conf matrix and save 
    print("\n",cl,"\n")
    print("Confusion matrix:\n",confusion_matrix(test_labels, pred))
    final_f1_micro[cl] = f1_score(test_labels, pred, average="micro")
    print("F1 micro score:",final_f1_micro[cl])
    final_f1_macro[cl] = f1_score(test_labels, pred, average="macro")
    print("F1 macro score:",final_f1_macro[cl])
    print("Metric scores and Averages:\n",classification_report(test_labels,pred))
    times_s[cl] = (round((t1-t0), 4), round((t2-t1), 4))

# print the fit and predict times in milliseconds
print("Fit and Predict times for non-dummy classfiers after optimization:")
for k,v in times_s.items():
    print (k, ":\n"," Fit time =", v[0],"\n Predict time =", v[1])
    
# Keeping values only for non-dummmy classifiers
baseline_fewer_f1_micro = {}
baseline_fewer_f1_macro = {}
for k in final_f1_macro.keys():
    baseline_fewer_f1_micro[k] = baseline_f1_micro[k]
    baseline_fewer_f1_macro[k] = baseline_f1_macro[k]

#### Bar plots

##Firstly for F1 Micro
names = list(final_f1_micro.keys())
prev_values = list(baseline_fewer_f1_micro.values())
values = list(final_f1_micro.values())
ind = np.arange(len(names))
width = 0.4
fig, ax = plt.subplots()
ax.barh(ind, prev_values, width, color='red', label='Baseline')
ax.barh(ind + width, values, width, color='green', label='Optimised')
plt.title('F1 Micro')
plt.ylabel('Classifiers')
plt.xlabel('Values')
ax.set(yticks=ind + width, yticklabels=names, ylim=[2*width - 1, len(names)])
plt.show()

##Firstly for F1 Macro
names = list(final_f1_macro.keys())
prev_values = list(baseline_fewer_f1_macro.values())
values = list(final_f1_macro.values())
ind = np.arange(len(names))
width = 0.4
fig, ax = plt.subplots()
ax.barh(ind, prev_values, width, color='red', label='Baseline')
ax.barh(ind + width, values, width, color='green', label='Optimised')
plt.title('F1 Macro')
plt.ylabel('Classifiers')
plt.xlabel('Values')
ax.set(yticks=ind + width, yticklabels=names, ylim=[2*width - 1, len(names)])
plt.show()

## Calculate change of perfomane before and after optimization
f1_micro_change = {}
f1_macro_change = {}
for key in final_f1_micro.keys():
    f1_micro_change[key] = final_f1_micro[key] - baseline_f1_micro[key]
    f1_macro_change[key] = final_f1_macro[key] - baseline_f1_macro[key]

print("Changes for classifiers for f1 micro :\n",f1_micro_change)
print("Changes for classifiers for f1 macro :\n",f1_macro_change)

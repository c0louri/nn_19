#OMADA A18
#
from time import time
import warnings
warnings.filterwarnings('ignore')
import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
from sklearn.dummy import DummyClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report,confusion_matrix,f1_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score, cross_validate
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from sklearn.preprocessing import MultiLabelBinarizer, StandardScaler, MinMaxScaler
from sklearn.feature_selection import VarianceThreshold
from sklearn.decomposition import PCA

## Code
## The function run_pipeline is used at the chapter Classifiers Optimization

def run_pipeline(parameters, train, train_labels, test, test_labels, estimator):
    (sc, os , pca_n, neighbors) = parameters
    ## Variance Threshold
    selector = VarianceThreshold()  #Initializing of a selector
    train_reduced = selector.fit_transform(train)  # Fit the transformer and transform the data for the training
    test_reduced = selector.transform(test)   # Transorm the data for the final testing
    ## Scaling
    if sc == 'standard':
        scaler = StandardScaler().fit(train_reduced)  # Initializing of Standard Scaler
        train_scaled = scaler.transform(train_reduced)
        test_scaled = scaler.transform(test_reduced)
    elif sc == 'min max':
        scaler = MinMaxScaler()  # Initializing of Min Max Scaler
        train_scaled = scaler.fit_transform(train_reduced)
        test_scaled = scaler.transform(test_reduced)
    elif sc == 'no':
        train_scaled = train_reduced
        test_scaled = test_reduced
    else:
        print("ERROR Scaling")
        return
    ## Sampling
    if os == 'over':
        ros = RandomOverSampler()  # Initializing of RandomOverSampler
        train_scaled_sampled, train_labels_sampled = ros.fit_sample(train_scaled, train_labels)
    elif os == 'under':
        rus = RandomUnderSampler()
        train_scaled_sampled, train_labels_sampled = rus.fit_resample(train_scaled, train_labels)
    elif os == 'no':
        train_scaled_sampled = train_scaled
        train_labels_sampled = train_labels
    else:
        print("ERROR Sampling")
        return
    ## PCA
    if pca_n == 0: # 0 -> no use of PCA
        train_PCA = train_scaled_sampled
        test_PCA = test_scaled
    elif pca_n > 0:
        pca = PCA(pca_n)
        # this transformation must be both on training and testing set, but the fitting only on training set
        train_PCA = pca.fit_transform(train_scaled_sampled)
        test_PCA = pca.transform(test_scaled)
    else:
        print("ERROR PCA")
        return
    # correct the names of the parameters for the classifiers (simpler)
    train = train_PCA
    train_labels = train_labels_sampled
    test = test_PCA
    ## Estimators
    if estimator == 'kNN':
        # K Neighbors Classifier
        knn = KNeighborsClassifier()
        t0 = time()
        knn.fit(train, train_labels)
        t1 = time()
        pred = knn.predict(test)
        t2 = time()
    elif estimator == 'uniform':
        # Uniform
        dc_uniform = DummyClassifier(strategy="uniform")
        t0 = time()
        model = dc_uniform.fit(train, train_labels)
        t1 = time()
        pred = dc_uniform.predict(test)
        t2 = time()
    elif estimator == 'constant 0':
        # Constant 0
        dc_constant_0 = DummyClassifier(strategy="constant", constant=0)
        t0 = time()
        model = dc_constant_0.fit(train, train_labels)
        t1 = time()
        pred = dc_constant_0.predict(test)
        t2 = time()
    elif estimator == 'constant 1':
        # Constant 1
        dc_constant_1 = DummyClassifier(strategy="constant", constant=1)
        t0 = time()
        model = dc_constant_1.fit(train, train_labels)
        t1 = time()
        pred = dc_constant_1.predict(test)
        t2 = time()
    elif estimator == 'most frequent':
        # Most Frequent
        dc_most_frequent = DummyClassifier(strategy="most_frequent")
        t0 = time()
        model = dc_most_frequent.fit(train, train_labels)
        t1 = time()
        pred = dc_most_frequent.predict(test)
        t2 = time()
    elif estimator == 'stratified':
        # Stratified
        dc_stratified = DummyClassifier(strategy="stratified")
        t0 = time()
        model = dc_stratified.fit(train, train_labels)
        t1 = time()
        pred = dc_stratified.predict(test)
        t2 = time()
    else:
        print("ERROR Estimator")
        return
    ## Return the predictions of the classifier on the test set
    fit_time = round((t1 - t0) * 1000, 3)
    predict_time = round((t2 - t1) * 1000, 3)
    return pred, fit_time, predict_time

df = pd.read_csv("plrx.txt", header=None, sep='\t' )
# in a second dataframe we keep only the collumns who matter(12 + 1)
df2 = df.iloc[:, :13]

features = df.iloc[:, :12]
print(features)
# save apart labels
labels = np.array(df.iloc[:, 12])
# encode label to 0,1
labels = labels.astype(int) - 1
print(labels)
# Frequency of different classes:
labels_size = labels.shape
counts = np.bincount(labels)
freqs = counts / labels_size
print("Frequency of 1.0 =", freqs[0]*100, "%")
print("Frequency of 2.0 =", freqs[1]*100, "%")

# Split dataset to train and test set
train, test, train_labels, test_labels = \
    train_test_split(features, labels, test_size=0.20)

## Baseline Classification
# Dictionary for the results of the Classifiers
baseline_f1_micro = {}
baseline_f1_macro = {}
# Uniform
dc_uniform = DummyClassifier(strategy="uniform")
model = dc_uniform.fit(train, train_labels)
pred = dc_uniform.predict(test)
# save confusion_matrix, f1 micro and f1 macro
uni_conf_matrix = confusion_matrix(test_labels, pred)
baseline_f1_micro['uniform'] = f1_score(test_labels, pred, average="micro")
baseline_f1_macro['uniform'] = f1_score(test_labels, pred, average="macro")
# print results:
print("Confusion matrix:\n" ,uni_conf_matrix, "\n")
print("F1_micro score:", baseline_f1_micro['uniform'])
print("F1_macro score: ", baseline_f1_macro['uniform'], "\n")
print("Metrics Scores and averages:\n", classification_report(test_labels, pred))

# Constant 0
dc_constant_0 = DummyClassifier(strategy="constant", constant=0)
model = dc_constant_0.fit(train, train_labels)
pred = dc_constant_0.predict(test)
# save confusion_matrix, f1 micro and f1 macro
const_0_conf_matrix = confusion_matrix(test_labels, pred)
baseline_f1_micro['constant 0'] = f1_score(test_labels, pred, average="micro")
baseline_f1_macro['constant 0'] = f1_score(test_labels, pred, average="macro")
# print results:
print("Confusion matrix:\n", const_0_conf_matrix, "\n")
print("F1_micro score:", baseline_f1_micro['constant 0'])
print("F1_macro score:", baseline_f1_macro['constant 0'], "\n")
print("Metrics Scores and averages:\n", classification_report(test_labels, pred))

# Constant 1
dc_constant_1 = DummyClassifier(strategy="constant", constant=1)
model = dc_constant_1.fit(train, train_labels)
pred = dc_constant_1.predict(test)
# save confusion_matrix, f1 micro and f1 macro
const_1_conf_matrix = confusion_matrix(test_labels, pred)
baseline_f1_micro['constant 1'] = f1_score(test_labels, pred, average="micro")
baseline_f1_macro['constant 1'] = f1_score(test_labels, pred, average="macro")
# print results:
print("Confusion matrix\n", const_1_conf_matrix, "\n")
print("F1_micro score:", baseline_f1_micro['constant 1'])
print("F1_macro score:", baseline_f1_macro['constant 1'], "\n")
print("Metrics Scores and averages:\n", classification_report(test_labels, pred))

# Most Frequent
dc_most_frequent = DummyClassifier(strategy="most_frequent")
model = dc_most_frequent.fit(train, train_labels)
pred = dc_most_frequent.predict(test)
# save confusion_matrix, f1 micro and f1 macro
most_freq_conf_matrix = confusion_matrix(test_labels, pred)
baseline_f1_micro['most frequent'] = f1_score(test_labels, pred, average="micro")
baseline_f1_macro['most frequent'] = f1_score(test_labels, pred, average="macro")
# print results:
print("Confusion matrix:\n" ,most_freq_conf_matrix, "\n")
print("F1_micro score:" ,baseline_f1_micro['most frequent'])
print("F1_macro score:" ,baseline_f1_macro['most frequent'], "\n")
print("Metrics Scores and averages:\n", classification_report(test_labels, pred))

dc_stratified = DummyClassifier(strategy="stratified")
model = dc_stratified.fit(train, train_labels)
pred = dc_stratified.predict(test)
# save confusion_matrix, f1 micro and f1 macro
strat_conf_matrix = confusion_matrix(test_labels, pred)
baseline_f1_micro['stratified'] = f1_score(test_labels, pred, average="micro")
baseline_f1_macro['stratified'] = f1_score(test_labels, pred, average="macro")
# print results:
print("Confusion matrix:\n" ,strat_conf_matrix, "\n")
print("F1_micro score:" ,baseline_f1_micro['stratified'])
print("F1 macro score:" ,baseline_f1_macro['stratified'], "\n")
print("Metrics Scores and averages:\n", classification_report(test_labels, pred))

# K Neighbors Classifier
knn = KNeighborsClassifier()
knn.fit(train, train_labels)
pred = knn.predict(test)
# save confusion_matrix, f1 micro and f1 macros
knn_conf_matrix = confusion_matrix(test_labels, pred)
baseline_f1_micro['kNN'] = f1_score(test_labels, pred, average="micro")
baseline_f1_macro['kNN'] = f1_score(test_labels, pred, average="macro")
# print results:
print("Confusion matrix:\n" ,knn_conf_matrix ,"\n")
print("F1_micro score:" ,baseline_f1_micro['kNN'])
print("F1_macro score:" ,baseline_f1_macro['kNN'] ,"\n")
print("Metrics Scores and averages:\n", classification_report(test_labels, pred))

# Bar plots
# Firstly for F1 Micro
names = list(baseline_f1_micro.keys())
values = list(baseline_f1_micro.values())
y_pos = np.arange(len(names))
plt.barh(y_pos, values, color=(0.5, 0.1, 0.5, 0.6))
plt.title('F1 Micro')
plt.ylabel('Classifiers')
plt.xlabel('Values')
plt.yticks(y_pos, names)
plt.show()

# Secondly for F1 Macro
names = list(baseline_f1_macro.keys())
values = list(baseline_f1_macro.values())
y_pos = np.arange(len(names))
plt.barh(y_pos, values)
plt.title('F1 Macro')
plt.ylabel('Classifiers')
plt.xlabel('Values')
plt.yticks(y_pos, names)
plt.show()

## Classifier Optimization
# Exhaustive search for best parameters with cross-validation
scaling = ['standard','min max','no']
oversampling = ['over', 'under', 'no']
pca_components = range(2, 13)
neighbors = list(filter(lambda x: x % 2 != 0, list(range(1, 12))))
#scoring = ['f1_micro', 'f1_macro']
f1_micro_scores = {}
f1_macro_scores = {}
# Variance Threshold
selector = VarianceThreshold()
train_reduced = selector.fit_transform(train)
for k in neighbors:
    # Scaling
    for sc in scaling:
        if sc == 'standard':
            scaler = StandardScaler().fit(train_reduced)
            train_scaled = scaler.transform(train_reduced)
        elif sc == 'min max':
            scaler = MinMaxScaler()
            train_scaled = scaler.fit_transform(train_reduced)
        elif sc == 'no':
            train_scaled = train_reduced
        # OverSampling
        for os in oversampling:
            if os == 'over':
                ros = RandomOverSampler()
                train_scaled_sampled, train_labels_sampled = ros.fit_resample(train_scaled, train_labels)
            elif os == 'under':
                rus = RandomUnderSampler()
                train_scaled_sampled, train_labels_sampled = rus.fit_resample(train_scaled, train_labels)
            elif os == 'no':
                train_scaled_sampled = train_scaled
                train_labels_sampled = train_labels
            # PCA
            for pca_n in pca_components:
                pca = PCA(pca_n)
                train_PCA = pca.fit_transform(train_scaled_sampled)
                #kNN
                knn = KNeighborsClassifier(n_neighbors=k)
                scores = cross_val_score(knn, train_PCA, train_labels_sampled, cv=10, scoring="f1_micro")
                f1_micro_scores[(sc, os, pca_n, k)] = round(scores.mean(), 5)
                knn = KNeighborsClassifier(n_neighbors=k)
                scores = cross_val_score(knn, train_PCA, train_labels_sampled, cv=10, scoring="f1_macro")
                f1_macro_scores[(sc, os, pca_n, k)] = round(scores.mean(), 5)

print("Scores for F1 micro :\n",f1_micro_scores)
print("\nScores for F1 macro :\n",f1_macro_scores)

# Find best combination for F1 Micro
vals_f1_micro = list(f1_micro_scores.values())
keys_f1_micro = list(f1_micro_scores.keys())
best_f1_micro = keys_f1_micro[vals_f1_micro.index(max(vals_f1_micro))]

# Find best combination for F1 Macro
vals_f1_macro = list(f1_macro_scores.values())
keys_f1_macro = list(f1_macro_scores.keys())
best_f1_macro = keys_f1_macro[vals_f1_macro.index(max(vals_f1_macro))]

# Print Results
# F1 Micro
print("Best combination for F1 Micro :", best_f1_micro, "with value =", f1_micro_scores[best_f1_micro])
# F1 Macro
print("Best combination for F1 Macro :", best_f1_macro, "with value =", f1_macro_scores[best_f1_macro])

## Calculate scores for wvery classifiers
best_param = best_f1_macro
print (best_f1_macro)
final_f1_micro = {}
final_f1_macro = {}
times_ms = {}

list_classifiers = ['uniform','constant 0', 'constant 1', 'most frequent', 'stratified', 'kNN']
for cl in list_classifiers:
    # run pipeline for cl estimator
    pred, fit_time, predict_time = run_pipeline(best_param, train, train_labels, test, test_labels, cl)
    # print conf matrix and save
    print("\n",cl,"\n")
    print("Confusion matrix:\n",confusion_matrix(test_labels, pred))
    final_f1_micro[cl] = f1_score(test_labels, pred, average="micro")
    print("F1 micro score:",final_f1_micro[cl])
    final_f1_macro[cl] = f1_score(test_labels, pred, average="macro")
    print("F1 macro score:",final_f1_macro[cl])
    print("Metric scores and Averages:\n",classification_report(test_labels,pred))
    times_ms[cl] = (fit_time, predict_time)
# print the fit and predict times in milliseconds
for k,v in times_ms.items():
    print (k, ":\n","Fit time =", v[0],"\n Predict time =", v[1])

# Comparison bar plots
##Firstly for F1 Micro
names = list(final_f1_micro.keys())
prev_values = list(baseline_f1_micro.values())
values = list(final_f1_micro.values())
ind = np.arange(len(names))
width = 0.4
fig, ax = plt.subplots()
ax.barh(ind, prev_values, width, color='red', label='Baseline')
ax.barh(ind + width, values, width, color='green', label='Optimised')
plt.title('F1 Micro')
plt.ylabel('Classifiers')
plt.xlabel('Values')
ax.set(yticks=ind + width, yticklabels=names, ylim=[2*width - 1, len(names)])
plt.show()

##Firstly for F1 Macro
names = list(final_f1_macro.keys())
prev_values = list(baseline_f1_macro.values())
values = list(final_f1_macro.values())
ind = np.arange(len(names))
width = 0.4
fig, ax = plt.subplots()
ax.barh(ind, prev_values, width, color='red', label='Baseline')
ax.barh(ind + width, values, width, color='green', label='Optimised')
plt.title('F1 Macro')
plt.ylabel('Classifiers')
plt.xlabel('Values')
ax.set(yticks=ind + width, yticklabels=names, ylim=[2*width - 1, len(names)])
plt.show()

# Calculate and store change of f1 micro and macro, before and after optimization
f1_micro_change = {}
f1_macro_change = {}
for key in final_f1_micro.keys():
    f1_micro_change[key] = final_f1_micro[key] - baseline_f1_micro[key]
    f1_macro_change[key] = final_f1_macro[key] - baseline_f1_macro[key]

print("Changes for classifiers for f1 micro :\n",f1_micro_change)
print("Changes for classifiers for f1 macro :\n",f1_macro_change)
